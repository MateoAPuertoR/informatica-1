var figuraS = 0;
var x1=0.0;
var y1=0.0;
var x2=0.0;
var y2=0.0;
var screen_w=screen.width;
var screen_h=screen.height;
var scala_w=1.0;
var scala_h=1.0;
var distancia=0.0;

function cambioFigura(idFigura) {
    figuraS = idFigura;
    return null;
}

function dibujar(x1,y1,x2,y2,w,h,r,x3,y3,ctx){    
	//ctx.scale(1.5,1.5);
    if(figuraS === 1){
        ctx.strokeRect(x1,y1,w,h);
    }else if(figuraS === 2){
		ctx.beginPath();
        ctx.arc(x1, y1, r, 0, Math.PI*2,true);
		ctx.stroke();
    }else if(figuraS===3){
        ctx.beginPath();
		ctx.moveTo(x1,y1);
		ctx.lineTo(x2,y2);
		ctx.lineTo(x3,y3);
		ctx.closePath();
		ctx.fill();
    }else{
		alert("Seleccione una figura");
	}
}

function limpiar(){
    var context  = document.getElementById('lienzo');		
	var ctx = context.getContext("2d");	
    ctx.clearRect(0,0,context.width,context.height);
	x1=0;
	y1=0;
	x2=0;
	y2=0;
}


var context  = document.getElementById('lienzo');
//scalar(context.width,context.height);
context.addEventListener("click",function(evt){	
	var ctx = context.getContext("2d");
	var mousePos = oMousePos(context, evt);
	marcarCoords(mousePos.x, mousePos.y,ctx)
});

function oMousePos(canvas, evt) {
  var ClientRect = canvas.getBoundingClientRect();
  return { //objeto
	x: Math.round(evt.clientX - ClientRect.left),
	y: Math.round(evt.clientY - ClientRect.top)
	//x: Math.round(evt.clientX),
	//y: Math.round(evt.clientY)
  }
}

function marcarCoords(x,y,ctx){
	debugger;
	if(x2!=0 && y2!=0){
		limpiar();
	}
	if(x1===0 && y1===0){
		x1=x*scala_w;
		y1=y*scala_h;
	}else{
		x2=x*scala_w;
		y2=y*scala_h;		
		
		var w=0.0;
		var h=0.0;
		var x3;
		var y3;
		var x4;
		var y4;
					
		if(figuraS === 1){
			if(x2>x1)w=x2-x1;
			else w=x1-x2;			
			if(y2>y1)h=y2-y1;		
			else h=y1-y2;
		}else if(figuraS === 2){
			if(x2>x1)x3=Math.pow((x2-x1),2);
			else x3=Math.pow((x1-x2),2);
			if(y2>y1)y3=Math.pow((y2-y1),2);
			else y3=Math.pow((y1-y2),2);
		}else if(figuraS===3){
			if(x2>x1)
			{
				x4=x2;
				w=x2-x1
				x3=Math.pow((x2-x1),2);
			}
			else 
			{
				x4=x1;
				w=x1-x2
				x3=Math.pow((x1-x2),2);
			}
			if(y2>y1)
			{
				y4=y1;
				h=y2-y1
				y3=Math.pow((y2-y1),2);
			}
			else
			{
				y4=y2;
				h=y1-y2;
				y3=Math.pow((y1-y2),2);
			}
		}
		distancia=Math.sqrt(x3+y3);
		dibujar(x1,y1,x2,y2,w,h,distancia,x4,y4,ctx);
		calculos(x1,y1,x2,y2,x3,y3,distancia,w,h);
	}	
}

function calculos(x1,y1,x2,y2,x3,y3,d,w,h){
	debugger;
	var area=0.0;
	var perimetro=0.0;
	if(figuraS === 1){
        area=w*h;
		perimetro=(w*2)+(h*2);
    }else if(figuraS === 2){
		area=Math.PI*Math.pow(d,2);
		perimetro=2*d*Math.PI;		
    }else if(figuraS===3){
        area=(w*h)/2;
		perimetro=w+h+d;
    }	
	document.getElementById("area").value=area+"px";
	document.getElementById("perimetro").value=perimetro+"px";
}



window.setTimeout(function() {
	Init();  
	window.addEventListener('resize', Init, false);
  }, 15);

  function Init(){
	// recalcula el tamaño del canvas
	context.width = parseInt(getComputedStyle(document.getElementById('area_trabajo')).getPropertyValue('width'));
	context.height = parseInt(getComputedStyle(document.getElementById('area_trabajo')).getPropertyValue('height'))-100;
			// limpia el canvas			
	}
	